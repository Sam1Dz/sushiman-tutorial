<div align="center">
  <h1 align="center">Sushiman</h3>
  
  <a href="https://youtu.be/QRrPE9aj3wI?feature=shared" target="_blank">
    <img src="https://github.com/adrianhajdin/project_html_css_website/assets/151519281/562e0f27-4b93-41cb-a63d-7c50940fc0ad" alt="Project Banner">
  </a>

  <div align="center">
    <img src="https://img.shields.io/badge/-HTML-black?style=for-the-badge&logoColor=white&logo=html5&color=E34F26" alt="html5" />
    <img src="https://img.shields.io/badge/-css-black?style=for-the-badge&logoColor=white&logo=css3&color=0277BD" alt="css3" />
    <img src="https://img.shields.io/badge/-vite_js-black?style=for-the-badge&logoColor=white&logo=vite&color=BD34FE" alt="vite" />
    <!-- <img src="https://img.shields.io/badge/-typescript-black?style=for-the-badge&logoColor=white&logo=typescript&color=007ACC" alt="typescript" />
    <img src="https://img.shields.io/badge/-tailwind_css-black?style=for-the-badge&logoColor=white&logo=tailwindcss&color=25BABD" alt="tailwind css" />
    <img src="https://img.shields.io/badge/-react_js-black?style=for-the-badge&logoColor=white&logo=react&color=00d8ff" alt="reactjs" /> -->
  </div>

  <br />

  <div align="center">
    Sushiman is a website that i follow based on Tutorial by <a href="https://www.youtube.com/@javascriptmastery/videos" target="_blank"><b>JavaScript Mastery</b></a> on his Youtube channel.
  </div>

  <br />

  <div align="center">
    <img src="https://img.shields.io/badge/-video_tutorial-black?style=for-the-badge&logoColor=white&logo=youtube&color=ff0000" alt="vite" />
  </div>
</div>

## Quick Start

Follow these steps to set up the project locally on your machine.

**Prerequisites**

Make sure you have the following installed on your machine:

- [Git](https://git-scm.com/)
- [Node.js](https://nodejs.org/en)
- [npm](https://www.npmjs.com/) (Node Package Manager)

or alternatively you can use [Yarn](https://yarnpkg.com/) or [Bun](https://bun.sh/) as a replacement for Node.js and npm.

**Cloning the Repository**

```bash
git clone https://gitlab.com/Sam1Dz/sushiman-tutorial.git
cd sushiman-tutorial
```

**Installation**

Install the project dependencies

```bash
# npm
npm install

# yarn
yarn install

# bun
bun install
```

**Running the Project**

```bash
# npm
npm run dev

# yarn
yarn run dev

# bun
bun run dev
```

Open [http://localhost:5173](http://localhost:5173) in your browser to view the project.

## Future Modification on this Project

Even though this is a project that i followed from the tutorial, I felt like i want to modifying it a little bit in the future like:

- Convert Javascript code to Typescript
- Use React JS framework instead of plain HTML
- Use Tailwind CSS instead of CSS
